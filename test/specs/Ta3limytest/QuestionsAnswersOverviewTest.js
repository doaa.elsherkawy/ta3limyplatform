describe('QuestionsAnswersOverviewTest', () => {   
    it('check that user is redirected to faq page when he clicks common question link at home screen', () => {
      browser.url(initial.ta3limy_url);
      RoosterPage.infoIcon().waitForDisplayed(60000);
      RoosterPage.infoIcon().click();
      BasePage.linkContainsText(TextsPage.commonQuestions()).waitForDisplayed(60000);
      BasePage.linkContainsText(TextsPage.commonQuestions()).click();
      AssertionsPage.url(TextsPage.faqURL());
    });
    it('check the header content of fa page', () => {
        AssertionsPage.checkElementAfterWait(RoosterPage.infoIcon());
        AssertionsPage.checkElementAfterWait(BasePage.linkContainsText(TextsPage.loginLink()));
        AssertionsPage.checkElementAfterWait(RoosterPage.appLogo());
        AssertionsPage.checkElementAfterWait(BasePage.headerValue(TextsPage.commonQuestions()));
      });
    it('check the questions and answers section of faq page', () => {
        AssertionsPage.checkTextPresence(BasePage.paragraphContainsText(TextsPage.firstQuestion()),TextsPage.firstQuestion());
        RoosterPage.AnswerArrowIcon(1).click();
        AssertionsPage.checkTextPresence(BasePage.paragraphContainsText(TextsPage.firstAnswer()), TextsPage.firstAnswer());
        RoosterPage.AnswerArrowIcon(1).click();
        AssertionsPage.checkTextPresence(BasePage.paragraphContainsText(TextsPage.secondQuestion()),TextsPage.secondQuestion());
        RoosterPage.AnswerArrowIcon(2).click();
        AssertionsPage.checkTextPresence(BasePage.paragraphContainsText(TextsPage.secondAnswer()), TextsPage.secondAnswer());
        RoosterPage.AnswerArrowIcon(2).click();
        AssertionsPage.checkTextPresence(BasePage.paragraphContainsText(TextsPage.thirdQuestion()),TextsPage.thirdQuestion());
        RoosterPage.AnswerArrowIcon(3).click();
        AssertionsPage.checkTextPresence(BasePage.paragraphContainsText(TextsPage.thirdAnswer()), TextsPage.thirdAnswer());
        RoosterPage.AnswerArrowIcon(3).click();
        RoosterPage.AnswerArrowIcon(1).scrollIntoView();
        AssertionsPage.checkTextPresence(BasePage.paragraphContainsText(TextsPage.fourthQuestion()),TextsPage.fourthQuestion());
        RoosterPage.AnswerArrowIcon(4).click();
        AssertionsPage.checkTextPresence(BasePage.paragraphContainsText(TextsPage.fourthAnswer()), TextsPage.fourthAnswer());
        RoosterPage.AnswerArrowIcon(4).click();
        AssertionsPage.checkTextPresence(BasePage.paragraphContainsText(TextsPage.fifthQuestion()),TextsPage.fifthQuestion());
        RoosterPage.AnswerArrowIcon(5).click();
        AssertionsPage.checkTextPresence(BasePage.paragraphContainsText(TextsPage.fifthAnswer()), TextsPage.fifthAnswer());
        RoosterPage.AnswerArrowIcon(5).click();
        AssertionsPage.checkTextPresence(BasePage.paragraphContainsText(TextsPage.sixQuestion()),TextsPage.sixQuestion());
        BasePage.paragraphContainsText(TextsPage.sixQuestion()).scrollIntoView();
        RoosterPage.AnswerArrowIcon(6).click();
        AssertionsPage.checkTextPresence(BasePage.paragraphContainsText(TextsPage.sixAnswer()), TextsPage.sixAnswer());
        browser.pause(2000);
        AssertionsPage.checkTextPresence(BasePage.paragraphContainsText(TextsPage.sevenQuestion()),TextsPage.sevenQuestion());
        RoosterPage.AnswerArrowIcon(7).click();
        AssertionsPage.checkTextPresence(BasePage.paragraphContainsText(TextsPage.sevenAnswer()), TextsPage.sevenAnswer()); 
      });

      it('check that footer and accessibility icon is displayed at faq page', () => {
        RoosterPage.AnswerArrowIcon(7).click(); browser.pause(5000);  
        AssertionsPage.checkElementAfterWait(RoosterPage.accessibilityIcon());
        AssertionsPage.checkElementAfterWait(RoosterPage.footer());
        AssertionsPage.checkElementAfterWait(BasePage.linkContainsText(TextsPage.privacyPolicy()));
        AssertionsPage.checkElementAfterWait(BasePage.linkContainsText(TextsPage.terms()));
      });
       
});


    
       
  
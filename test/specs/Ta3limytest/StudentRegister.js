let phoneNumber='01' + Math.floor(100000000 + Math.random() * 900000000);
describe('StudentRegister', () => {   
    it('check that student is redirected to registeration page when he clicks register button at home screen', () => {
      browser.url(initial.ta3limy_url);browser.pause(4000);
      BasePage.linkContainsText(TextsPage.register()).waitForDisplayed(60000);
      BasePage.linkContainsText(TextsPage.register()).click();
      AssertionsPage.url(TextsPage.registerURL());
    });

    it('check that student will register successfully when he fills all mandatory fields at register page', () => {
        RoosterPage.studentCheckbox().waitForDisplayed(60000);
        RoosterPage.studentCheckbox().click();
        RoosterPage.firstName().setValue(TextsPage.studentFirstName());
        RoosterPage.lastName().setValue(TextsPage.studentLastName());
        RoosterPage.lastName().scrollIntoView();
        RoosterPage.phoneNumber().setValue(phoneNumber);
        RoosterPage.gendar().click();
        RoosterPage.gendar().scrollIntoView();
        RoosterPage.gradeDropdown().click();
        RoosterPage.gradeValue().click();
        RoosterPage.password().scrollIntoView();
        RoosterPage.password().setValue(TextsPage.defaultPasswordValue());
        RoosterPage.passwordConfirmation().setValue(TextsPage.defaultPasswordValue());
        // RoosterPage.termsAndPolicy().click({ x:4},{y:4} );
        // browser.pause(4000);
        // // RoosterPage.amNotRobotFrame().waitForDisplayed(60000);
        // // browser.switchToFrame(RoosterPage.amNotRobotFrame());
        // // RoosterPage.amNotRobotCheckbox().click();
        // // browser.switchToParentFrame();
        // BasePage.linkContainsText(TextsPage.register()).click();  
      });
       
});


    
       
  
'use strict';

class TextsPage extends Page {

commonQuestions() {
   return browser.text= "أسئلة شائعة";
}

faqURL() {
    return browser.text= "https://www.ta3limy.com/faq";
 }

 loginLink() {
    return browser.text= "الدخول الي الحساب";
 }
 firstQuestion() {
    return browser.text= "ما هي المؤسسة ؟";
 }
 secondQuestion() {
    return browser.text= "ما هو تعليمي ؟";
 }
 thirdQuestion() {
    return browser.text= "من المستفيد من تعليمي ؟";
 }
 fourthQuestion() {
    return browser.text= "ما هو المحتوي الموجود في تعليمي؟";
 }
 fifthQuestion() {
    return browser.text= "هل تعليمي مجاني ؟";
 }
 sixQuestion() {
    return browser.text= "كيف أقوم باستخدام تعليمي ؟";
 }
 sevenQuestion() {
    return browser.text= "كيف يتم احتساب النقاط ؟";
 }
 firstAnswer() {
    return browser.text= "مؤسسة ڤودافون مصر لتنمية المجتمع هي أول مؤسسة لا تهدف للربح في مجال الاتصالات في مصر وهي تابعة لوزارة التضامن الاجتماعي وتختلف عن قسم المسؤولية المجتمعية الخاص بالشركة. من وقت تأسيسها في سنة 2003 والمؤسسة بتحاول جاهدة إنها تنمي المجتمع من خلال إشراك منظمات المجتمع المدني والهيئات الغير حكومية في عملية تطوير المجتمع المصري في مجالات التعليم وتمكين ذوي الاحتياجات الخاصة.";
 }
 secondAnswer() {
    return browser.text= "تعليمي هي منصة إلكترونية تعليمية مجانية للطلاب وأولياء الأمور والمعلمين";
 }
 thirdAnswer() {
    return browser.text= "أولياء الأمور والطلاب في المرحلة الأولي وستستهدف المرحلة القادمة تقديم محتوى للمعلمين";
 }
 fourthAnswer() {
    return browser.text= "لأولياء الأمور يوجد:";
 }
 fifthAnswer() {
    return browser.text= "تعليمي مجاني لجميع الأفراد بشكل عام ويتميز عملاء ڤودافون بعدم السحب من باقة الانترنت بشكل خاص";
 }
 sixAnswer() {
    return browser.text= "في حال لم يكن لديك حساب:";
 }
 sevenAnswer() {
    return browser.text= "يتم تحديد الأشخاص الموجودين بلوحة المتصدرين على منصة تعليمي عن طريق الطرق التالية:";
 }

 privacyPolicy() {
    return browser.text= "سياسة الخصوصية";
 }

 terms() {
    return browser.text= "الشروط و الأحكام";
 }

 register() {
    return browser.text= "تسجيل حساب";
 }

 registerURL() {
    return browser.text= "https://www.ta3limy.com/register";
 }
 
 registerAsStudent() {
    return browser.text= "طالب";
 }

 defaultPasswordValue() {
    return browser.text  = "12345678aA!";
} 

studentFirstName() {
    return browser.text= "test1";
 }

 studentLastName() {
    return browser.text= "test2";
 }


}




module.exports = new TextsPage();

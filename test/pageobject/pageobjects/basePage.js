'use strict';

class BasePage extends Page {
    
    headerValue(text) {
        return $("//h1[contains(.,'"+text+"')]");
    } 
    
    linkContainsText(text) {
        return $("//a[contains(.,'" + text + "')]");
    }
       
    paragraphContainsText(text) {
        return $("//p[contains(.,'" + text + "')]");
    }
    

}

module.exports = new BasePage();    
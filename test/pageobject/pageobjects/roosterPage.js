'use strict';

class RoosterPage extends Page {

    infoIcon() {
        return $("#help-menu");
    }

    appLogo(){
        return $("//header//img");
    }

     accessibilityIcon() {
       return $("#userwayAccessibilityIcon");
    }
    AnswerArrowIcon(i)
    {
        return $("(//div/span/p/i)[" + i + "]");
   
    }

    footer()
    {
        return $("//footer[@class='css-ni8xtt e1bg2hqq1']");
   
    }

    studentCheckbox() {
        return $("//*[@id='student']/../label");
    }

    firstName() {
        return $("#firstName");
    }

    lastName() {
        return $("#lastName");
    }

    phoneNumber() {
        return $("#mobileNumber");
    }

   gendar() {
        return $("//*[@id='male']/../label");
    }

    gradeDropdown() {
        return $("#grade");
    }

    gradeValue() {
        return $("//option[contains(.,'الصف الأول - الابتدائي')]");
    }

    password() {
        return $("#password");
    }

    passwordConfirmation() {
        return $("#passwordConfirmation");
    }
    termsAndPolicy() {
        return $("//span[contains(.,'اوافق على ')]");
    }

    amNotRobotFrame() {
        return $("//iframe[contains(@src,'https://www.google.com/recaptcha/api2/anchor')]");
    }
    amNotRobotCheckbox() {
        return $("//span[@class='recaptcha-checkbox goog-inline-block recaptcha-checkbox-unchecked rc-anchor-checkbox recaptcha-checkbox-hover']");
    }
    

}


module.exports = new RoosterPage();

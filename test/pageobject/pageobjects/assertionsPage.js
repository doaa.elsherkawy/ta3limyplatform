'use strict';

class AssertionsPage extends Page {
    
    url(text) {           
        assert.equal(browser.getUrl(), text);    
    }
	    checkElementAfterWait(element, error) {
       expect(element).toBeVisible({ message: error });
   } 
       checkTextPresence(element, text) {
       expect(element).toHaveTextContaining(text);
   }
      
}

module.exports = new AssertionsPage();
const tasks = require('./tasks');

const { print, COLORS } = tasks.runner;

const error = (err) => {
    print(err.message, COLORS.error);
};

const { setEnvironment, seleniumServer, wdio } = tasks;

setEnvironment().then(seleniumServer).then(wdio).catch(error);

# Pre-installation
NodeJS (node 8.11.2, npm 5.6.0) and Java should be installed on your local machine.
# Installation
Clone this folder to your local machine with a command
```
//clone project
$ git clone <repo's url>
```
Go to this folder in terminal and run

```
//install project
$ npm install
```

# Running the project
In the `package.json` file, please choose against which product/environment tests should be run
```
//choose a config that includes tests for specific project/environment
  "scripts": {
    "make": "node index.js",
    "make-local": "node index.js --local",
    "test_ta3limy_staging_staging_regression": "wdio configs/wdio.ta3limy.full.dev.conf.js"
  },
```
For example we want to choose a set of smoke tests for uCoach-rooster project in staging enironment, so all we need to do is to specify config name after `npm run` command
```
//run tests
$ npm run test_ta3limy_staging_staging_regression
```
or we can run with `make` command
```
npm run make config=./configs/wdio.rooster.dev.conf.js
```
# Switch from Browserstack to Local execution
Chromedriver and Selenuim standalone server are now part of the npm install. To run locally
```
npm run make-local config=./configs/wdio.rooster.dev.conf.js
```

There is another way to switch from Browserstack to local execution. First that need to do is do download selenium standalone server https://www.seleniumhq.org/download/
Then download chromedriver http://chromedriver.chromium.org/downloads
After it comment browserstack settings
```
//comment browserstack settings in config
    //Browserstack service settings 
//    services: ['browserstack'],
//    user: 'kosta10',
//    key:  'pXcyQxxSycb2zqsZkmdW',
```
Start selenium server with a command in terminal
```
//start selenium server
$ java -Dwebdriver.gecko.driver="/Users/kosta/curl/chromedriver.exe" -jar /Users/kosta/curl/selenium-server-standalone-3.13.0.jar
```
If a message `Selenium Server is up and running on port 4444` appears in console then selenium server is up and running
Now you are able to run auto tests the same way as mentioned in previous section with `npm run` command.

# Local Support Update

Newer syntax for calls will provide support for config files to exist externally.

To set config location and global vars for tests
```
// this command uses a trimmed config that doesn't bootstrap global.initial for tests. those are now loaded by the config
npm run make-local config=./configs/wdio.rooster.dev.conf.external-initial.js vars=./test/global/initial-external.js

// NOTE: validation functions should be added to these vars files to ensure required env vars for the tests they run are correct
```

# Running different test suites
By default `npm run test_ucoach-rooster_staging_staging_smoke`command would run all tests specified in config
```
//list of tests specified in config
    specs: [

    ],
```
Tests can be divided into suites in config
```
       
        // './test/specs/**/QuestionsAnswersOverviewTest.js',
        // './test/specs/**/StudentRegister.js',
    },
```


# Running selected tests
In some cases, you may wish to only execute a single test. With the `--spec` parameter you can do it.
```
//run the selected test
$ npm run test_ucoach-rooster_staging_staging_smoke -- --spec ./test/specs/**/VoicesOverviewTest.js
```
```
//run the selected test with `make`
$ npm run make config=./configs/wdio.rooster.dev.conf.js spec=./test/specs/RoosterTests/OpenMessageLibraryTest.js
```
# NPM & Third Party Libraries
This is our code please funnel suggestions through konstantin.zalutsky@insidetrack.com
# Environment
OSX currently, if you choose Windows run at own risk.

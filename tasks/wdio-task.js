const path = require('path');
const jv = require('javascript-validator');
const { execute } = require('./runner');

module.exports = () => {
    const { local, configPath, envVarRef } = global;
    const env = { local: local, configPath: configPath, envVarRef: envVarRef };

    if (!jv(envVarRef).isNonEmptyString.result) { 
        delete env.envVarRef;
    }

    if(!local) { 
        delete env.local;
    }

    const hasSuites = jv(global.suites).isNonEmptyString.result;
    const hasSpec = jv(global.spec).isNonEmptyString.result; 

    const args = [path.resolve('./configs/wdio.current.config')];

    if (hasSuites) {
        args.push('--suite');
        args.push(global.suites);
    }

    if (hasSpec) {
        args.push('--spec');
        args.push(global.spec);
    }

    const options = {
        title: 'webdriver',
        command: './node_modules/.bin/wdio',
        args: args,
        env: env
    };

    return execute(options);
}

const path = require('path');
const childProcess = require('child_process');

const seleniumServer = require('selenium-server');
const chromedriver = {
    path: path.resolve('../node_modules/chromedriver/bin/chromedriver.exe')
};

const { print } = require('./runner');

module.exports = () => {
    return new Promise((resolve) => {
        const { exec } = childProcess;
        const command = `java -Dwebdriver.gecko.driver="${chromedriver.path}" -jar ${seleniumServer.path}`;
        const args = [];
    
        if(local) { 
            print('run chromedriver and selenium server');
            
            const java = exec(command, args);
        
            java.stdout.on('data', (data) => {
                process.stderr.write(data);
            });
        
            java.stderr.on('data', (data) => {
                process.stderr.write(data);
            });
            resolve();
        } else { 
            resolve();
        }
    });
};

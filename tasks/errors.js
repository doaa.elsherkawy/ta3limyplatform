const { print, COLORS } = require('./runner');

module.exports = () => new Promise((resolve, reject) => {
    print(`\terror for executing undefined process. \n`, COLORS.error);
    reject(new Error('undefined task'));
});

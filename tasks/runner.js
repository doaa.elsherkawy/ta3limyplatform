const childProcess = require('child_process');

const CONSOLE_COLORS = {
	error: '\x1B[31m',
	warning: '\x1B[33m',
	info: '\x1B[32m',
	normal: '\x1B[36m',
	basic: '\x1B[39m'
};
const COLORS = {
	error: 'error',
	warning: 'warning',
	info: 'info',
	normal: 'normal',
	basic: 'basic'
};

function print(msg, type) {
	const color = CONSOLE_COLORS[type] ? CONSOLE_COLORS[type] : CONSOLE_COLORS.normal;
	console.log(`${color} ${msg} ${CONSOLE_COLORS.basic}`);
}

function execute(title, command, args, env) {
	let instructions = [];
	print(`Running task: ${title}`);

	instructions.push(command);
	if (args instanceof Array) {
		instructions = instructions.concat(args);
    }

    print(`wdio options: ${instructions}`);

	return new Promise((resolve, reject) => {
        const { spawn } = childProcess;
        const child = spawn(process.execPath, instructions, {
            env: env,
            stdio: 'inherit',
            cwd: process.cwd()
        });

        child.on('error', (err) => {
            print(`\terror for ${title} \n`, COLORS.error);
            reject(err);
        });

        child.on('close', (code) => {
            print(`\t${title} completed \n`, COLORS.info);
            resolve(code);
        });
	});
}

exports.execute = ({ title = 'error task', command = 'node', args = ['./tasks/error.js'], env = {} }) => {
	return execute(title, command, args, env);
};
exports.print = print;
exports.COLORS = COLORS;
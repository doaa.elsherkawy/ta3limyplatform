const path = require('path');
const jv = require('javascript-validator');

const { print } = require('./runner');

const fetchArg = (key, some) => {
    let pair; 

    if(some) {
        pair = process.argv.some(val => val === key);
    } else { 
        process.argv.forEach((val) => { 
            const found = (val.indexOf(key) !== -1);
        
            if(found) {
                const items = val.split(key);
                if (jv(items).isArray.result && items.length === 2) {
                    pair = items[1];
                }
            }
        });
    }

    return pair;
};

module.exports = () => new Promise((resolve, reject) => {
    print('fetching parameters');

    const configPath = fetchArg('config=');
    const suites = fetchArg('suite=');
    const spec = fetchArg('spec=');

    global.envVarRef = fetchArg('vars=');
    global.local = fetchArg('--local', true);

    if (jv(suites).isNonEmptyString.result) { 
        const items = suites.split(',');
        if (items.length > 0) {
            global.suites = suites;
        }
    }

    if (jv(spec).isNonEmptyString.result) {
        if (spec.split(',').length === 1) {
            global.spec = spec;
        }
    }
    
    if (!jv(configPath).isNonEmptyString.result) {
        reject('invalid configuration path');
    } else {
        global.configPath = path.resolve(configPath);

        print('parameters set');
        resolve();
    }
});


const path = require('path');
const jv = require('javascript-validator');

const { local, configPath, envVarRef } = process.env;

const definition = require(configPath);
const { config } = definition;

if(local) { 
    delete config.services;
    delete config.user;
    delete config.key;
}

let validated;

if (jv(envVarRef).isNonEmptyString.result) { 
    envVars = require(path.resolve(envVarRef));
    validated = (jv(envVars).has('validate').result && jv(envVars.validate).isFunction.result) ? envVars.validate() : false;
    global.initial = envVars.initial;
} else { 
    envVars = require(path.resolve('./test/global/initial.js'));
    // default tests should be validated
    global.initial = envVars;
    validated = true;
}

if (jv(envVars).isObject.result && jv(envVars).has('validate').result) {
    validated = envVars.validate();
}

if(!validated) { 
    throw Error('configuration missing validation');
}

exports.config = config;

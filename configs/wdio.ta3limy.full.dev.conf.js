exports.config = {
    // Browserstack service settings
    // services: ['browserstack'],
    // user: 'mohamedmedhat6',
    // key:  'NyWexAjsqdPm25Br8WqZ',
	
    specs: [
 
         //'./test/specs/**/QuestionsAnswersOverviewTest.js',
         './test/specs/**/StudentRegister.js',
        
    ],

    suites: {
    },

    maxInstances: 5,
    // specFileRetries: 2,
    // specFileRetriesDeferred: false,

    // If you have trouble getting all important capabilities together, check out the
    // Sauce Labs platform configurator - a great tool to configure your capabilities:
    // https://docs.saucelabs.com/reference/platforms-configurator
    //
    capabilities: [{
        browserName: 'chrome',
        'chromeOptions': {
            'args': [
                'use-fake-ui-for-media-stream',
                'use-fake-device-for-media-stream',
                "start-maximized",
                "disable-webgl",
                "blacklist-webgl",
                "blacklist-accelerated-compositing",
                "disable-accelerated-2d-canvas",
                "disable-accelerated-compositing",
                "disable-accelerated-layers",
                "disable-accelerated-plugins",
                "disable-accelerated-video",
                "disable-accelerated-video-decode",
                "window-size=2048,1536",
                "disable-gpu",
                "disable-infobars",
                "test-type",
                "disable-extensions",
                // "headless",
            ]
        },
        'os': 'Windows',
        'os_version': '10',
        'browser': 'Chrome',
        'resolution': "2048x1536",
        'build': "UCP Regression: Try Failed Tests" + ": " + (new Date()).toDateString(),
        // 'build': "UCP_Staging_Regression" + ": " + (new Date()).toDateString(),
        // 'build': "UCP Reg. Failed Tests No. 1: " + (new Date()).toDateString(),
        'project': 'ucoach_rooster_regression',
        'IDLETIMEOUT': '240000',
        // 'IDLETIMEOUT': '180000',
        //'browserstack.debug': true,

        /*tags: ['examples'],
        name: 'Chrome',
        // If using Open Sauce (https://saucelabs.com/opensauce/),
        // capabilities must be tagged as "public" for the jobs's status
        // to update (failed/passed). If omitted on Open Sauce, the job's
        // status will only be marked "Finished." This property can be
        // be omitted for commerical (private) Sauce Labs accounts.
        // Also see https://support.saucelabs.com/customer/portal/articles/2005331-why-do-my-tests-say-%22finished%22-instead-of-%22passed%22-or-%22failed%22-how-do-i-set-the-status-
        'public': true */
    }/*,
	{
        browserName: 'firefox',
	    platform: 'win7',
    },*//*
	{
        browserName: 'internet explorer',
        version: '11',
        platform: 'win7',
        version: '11.0.9600.18499IS',
    }*//*,
	{
        browserName: 'safari',
        platform: 'Mac'
    },*//*
	{
        browserName: 'internet explorer',
        platform: 'win8.1',
    },
	{
        browserName: 'MicrosoftEdge',
        platform: 'win10',
    }*/
    ],
    //
    // ===================
    // Test Configurations
    // ===================
    // Define all options that are relevant for the WebdriverIO instance here
    //
    // By default WebdriverIO commands are executed in a synchronous way using
    // the wdio-sync package. If you still want to run your tests in an async way
    // e.g. using promises you can set the sync option to false.
    sync: true,
    //
    // Level of logging verbosity: silent | verbose | command | data | result | error
    logLevel: 'silent',
    // If you only want to run your tests until a specific amount of tests have failed use
    // bail (default is 0 - don't bail, run all tests).
    //bail: 2,
    //
    // Enables colors for log output.
    coloredLogs: true,
    //
    // Disables deprecation warnings.
    deprecationWarnings: false,
    //
    // Saves a screenshot to a given path if a command fails.
    screenshotPath: './errorShots/',
    //
    // Set a base URL in order to shorten url command calls. If your url parameter starts
    // with "/", then the base url gets prepended.
    baseUrl: 'http://localhost',
    //
    // Default timeout for all waitFor* commands.
    waitforTimeout: 50000,
    //
    // Default timeout in milliseconds for request
    // if Selenium Grid doesn't send response
    connectionRetryTimeout: 120000,
    //
    // Default request retries count
    connectionRetryCount: 2,
    //
    // Initialize the browser instance with a WebdriverIO plugin. The object should have the
    // plugin name as key and the desired plugin options as properties. Make sure you have
    // the plugin installed before running any tests. The following plugins are currently
    // available:
    // WebdriverCSS: https://github.com/webdriverio/webdrivercss
    // WebdriverRTC: https://github.com/webdriverio/webdriverrtc
    // Browserevent: https://github.com/webdriverio/browserevent
    // plugins: {
    //     webdrivercss: {
    //         screenshotRoot: 'my-shots',
    //         failedComparisonsRoot: 'diffs',
    //         misMatchTolerance: 0.05,
    //         screenWidth: [320,480,640,1024]
    //     },
    //     webdriverrtc: {},
    //     browserevent: {}
    // },
    //
    // Test runner services
    // Services take over a specific job you don't want to take care of. They enhance
    // your test setup with almost no effort. Unlike plugins, they don't add new
    // commands. Instead, they hook themselves up into the test process.
    // services: [],//
    // Framework you want to run your specs with.
    // The following are supported: Mocha, Jasmine, and Cucumber
    // see also: http://webdriver.io/guide/testrunner/frameworks.html

    // Make sure you have the wdio adapter package for the specific framework installed
    // before running any tests.
    framework: 'mocha',
    //
    // Test reporter for stdout.
    // The only one supported by default is 'dot'
    // see also: http://webdriver.io/guide/testrunner/reporters.html
    reporters: ['spec'],
    //
    // Options to be passed to Mocha.
    // See the full list at http://mochajs.org/
    mochaOpts: {
        ui: 'bdd',
        // timeout: 180000
        timeout: 480000
    },
    //
    // =====
    // Hooks
    // =====
    // WebdriverIO provides several hooks you can use to interfere with the test process in order to enhance
    // it and to build services around it. You can either apply a single function or an array of
    // methods to it. If one of them returns with a promise, WebdriverIO will wait until that promise got
    // resolved to continue.
    //
    // Gets executed once before all workers get launched.
    // onPrepare: function (config, capabilities) {
    // },
    beforeSession: function (config, capabilities, specs) {
        require('expect-webdriverio');
        capabilities.name = specs[0].split('/').pop() || undefined;
        throw new Error("This test is failed before running");
    },
    //
    // Gets executed before test execution begins. At this point you can access all global
    // variables, such as `browser`. It is the perfect place to define custom commands.
    before: function (capabilities, specs) {
        var d = new Date();
        var n = d.getTime();
        global.n = n;
        var a = Math.floor((Math.random() * 100000) + 1);
        global.a = a;
        var random_name_generator = require('random-name');
        var randomFirstName = random_name_generator.first();
        global.randomFirstName = randomFirstName;
        var randomLastName = random_name_generator.last();
        global.randomLastName = randomLastName;
        var today = new Date();
        today.setDate(today.getDate());
        today.toISOString();
        global.today = today;
        var tomorrow = new Date();
        tomorrow.setDate(tomorrow.getDate() + 1);
        tomorrow.toISOString();
        global.tomorrow = tomorrow;
        var afterTomorrow = new Date();
        afterTomorrow.setDate(afterTomorrow.getDate() + 2);
        afterTomorrow.toISOString();
        global.afterTomorrow = afterTomorrow;
        var assert = require('assert');
        global.assert = assert;
        var Page = require('../test/pageobject/pageobjects/page.js');
        global.Page = Page;
        var RoosterPage = require('../test/pageobject/pageobjects/roosterPage.js');
        global.RoosterPage = RoosterPage;
        var TextsPage = require('../test/pageobject/pageobjects/textsPage.js');
        global.TextsPage = TextsPage;
        var AssertionsPage = require('../test/pageobject/pageobjects/assertionsPage.js');
        global.AssertionsPage = AssertionsPage;
        var BasePage = require('../test/pageobject/pageobjects/basePage.js');
        global.BasePage = BasePage;
        var initial = require('../test/global/initial.js');
        global.initial = initial;
        browser.maximizeWindow();
       
    },
    //
    // Hook that gets executed before the suite starts
    // beforeSuite: function (suite) {
    // },
    //
    // Hook that gets executed _before_ a hook within the suite starts (e.g. runs before calling
    // beforeEach in Mocha)
    // beforeHook: function () {
    // },
    //
    // Hook that gets executed _after_ a hook within the suite starts (e.g. runs after calling
    // afterEach in Mocha)
    // afterHook: function () {
    // },
    //afterEach:(function(done) {
    //    browser.end(done);
    //}),
    //
    // Function to be executed before a test (in Mocha/Jasmine) or a step (in Cucumber) starts.
    // beforeTest: function (test) {
    // },
    //
    // Runs before a WebdriverIO command gets executed.
    // beforeCommand: function (commandName, args) {
    // },
    //
    // Runs after a WebdriverIO command gets executed
    // afterCommand: function (commandName, args, result, error) {
    // },
    //
    // Function to be executed after a test (in Mocha/Jasmine) or a step (in Cucumber) starts.
    // afterTest: function (test) {
    // },
    //
    // Hook that gets executed after the suite has ended
    // afterSuite: function (suite) {
    // },
    //
    // Gets executed after all tests are done. You still have access to all global variables from
    // the test.
    // after: function (capabilities, specs) {
    // },
    //
    // Gets executed after all workers got shut down and the process is about to exit. It is not
    // possible to defer the end of the process using a promise.
    // onComplete: function(exitCode) {
    // }
}